//
//  DropZoneNSView.m
//  Box Monster
//
//  Created by Maciej Komorowski on 20.04.2013.
//  Copyright (c) 2013 Maciej Komorowski. All rights reserved.
//

#import "DropZoneNSView.h"
#import "AppDelegate.h"
#import <DropboxOSX/DropboxOSX.h>


@implementation DropZoneNSView

@synthesize pboard;
@synthesize fileURL;


- (id)initWithFrame:(NSRect)frame
{
    // Init self
    self = [super initWithFrame:frame];
    if ( ! self )
    {
		NSLog(@"Error: MyNSView initWithFrame");
        return self;
    }
    
    // Init object values 
    fileURL = nil; // Assume this exists
    pboard = [NSPasteboard pasteboardWithName:NSDragPboard];
    

    // Register Drag and Drop feature
    [self registerForDraggedTypes:[NSArray arrayWithObjects: NSURLPboardType, nil]];
    
    return self;
}  // end initWithFrame


- (NSDragOperation)draggingEntered:(id <NSDraggingInfo>)sender
{
    return NSDragOperationCopy;
}

- (BOOL)performDragOperation:(id <NSDraggingInfo>)sender
{
    // Get paste board
	pboard = [sender draggingPasteboard];
	    
    // Get file url from paste board
    NSURL *fileUrl = [NSURL URLFromPasteboard:pboard];
    
    // Upload files
    ([(AppDelegate *) [[NSApplication sharedApplication] delegate] uploadFile:fileUrl]);
    
    return YES;
}




- (void)drawRect:(NSRect)dirtyRect {
        
    //  Set image of view
    NSImage *imgMonster = [NSImage imageNamed:@"monster.png"];
    [imgMonster drawInRect:dirtyRect fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
}

@end
