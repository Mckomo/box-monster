//
//  DropZoneNSView.h
//  Box Monster
//
//  Created by Maciej Komorowski on 20.04.2013.
//  Copyright (c) 2013 Maciej Komorowski. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface DropZoneNSView : NSView

@property (assign) NSURL *fileURL;
@property (assign) NSPasteboard *pboard;

@end
