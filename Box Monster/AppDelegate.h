//
//  AppDelegate.h
//  Box Monster
//
//  Created by Maciej Komorowski on 20.04.2013.
//  Copyright (c) 2013 Maciej Komorowski. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <DropboxOSX/DropboxOSX.h>

@interface AppDelegate : NSObject <NSApplicationDelegate> {
    NSPasteboard *pboard;
    DBRestClient *restClient;
}

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet NSButton *linkButton;

- (void) registerHotkey;
- (void) hotkeyWithEvent:(NSEvent *)hkEvent;
- (IBAction)didPressLink:(id)sender;
- (void)uploadFile:(NSURL *)fileUrl;
- (void)updateLinkButton;
- (DBRestClient *)restClient;


@end
