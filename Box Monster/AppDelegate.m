//
//  AppDelegate.m
//  Box Monster
//
//  Created by Maciej Komorowski on 20.04.2013.
//  Copyright (c) 2013 Maciej Komorowski. All rights reserved.
//

#import "AppDelegate.h"

#import <DropboxOSX/DropboxOSX.h>
#import <Growl/Growl.h>

#import "DDHotKeyCenter.h"


@implementation AppDelegate

@synthesize window;
@synthesize linkButton;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    
    [self registerHotkey];
    
    // Bring app up front
    [NSApp activateIgnoringOtherApps:YES];
    
    // Init paste board for future link copying
    pboard = [NSPasteboard generalPasteboard];
    [pboard declareTypes:[NSArray arrayWithObject:NSStringPboardType] owner:self]; // Allow strings
    
    // Init Dropbox session
    DBSession* dbSession = [[DBSession alloc]
                            initWithAppKey:@"22khkp27tgb8wqw"
                            appSecret:@"cyn80x5mww4vmii"
                            root:kDBRootAppFolder];
    [DBSession setSharedSession:dbSession];
    
    // Register hook for auth state change
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(authHelperStateChangedNotification:)
     name:DBAuthHelperOSXStateChangedNotification
     object:[DBAuthHelperOSX sharedHelper]];
    
    // Register hook for "Show App" button click
    NSAppleEventManager *em = [NSAppleEventManager sharedAppleEventManager];
    [em setEventHandler:self andSelector:@selector(getUrl:withReplyEvent:)
          forEventClass:kInternetEventClass andEventID:kAEGetURL];
    
    [self updateLinkButton];
    
    // If connected to Dropbox
    if ( [[DBSession sharedSession] isLinked] )
    {
        // Show notification with instruction
        [GrowlApplicationBridge notifyWithTitle:@"Just drag and drop!"
                                    description:@"Drop files on monster and wait for link to the file."
                                    notificationName:@"aBoxMonsterInstruction"
                                    iconData:nil
                                    priority:0
                                    isSticky:NO
                                    clickContext:nil];
        
    }
    else
    {
        // Show notification with linking Dropbox request.
        [GrowlApplicationBridge notifyWithTitle:@"Box Moster says hi!"
                                    description:@"Clik link button to connect app with your Dropbox account."
                                    notificationName:@"aBoxMonsterLinkDropbox"
                                    iconData:nil
                                    priority:0
                                    isSticky:NO
                                    clickContext:nil];
    }
    
}

- (void)registerHotkey {
    DDHotKeyCenter * hotkeyCenter = [[DDHotKeyCenter alloc] init];
    if ( ![hotkeyCenter registerHotKeyWithKeyCode:46 modifierFlags:NSControlKeyMask target:self action:@selector(hotkeyWithEvent:) object:nil] ) {
        NSLog(@"unable to register hotkey");
    } else {
        NSLog(@"registered hotkey");
    }
}

- (void) hotkeyWithEvent:(NSEvent *)hkEvent {
    NSLog(@"Monster revived!", hkEvent);
    // Bring app active and window up front
    [NSApp activateIgnoringOtherApps:YES];
    [window makeKeyAndOrderFront:nil];
}

// Connect/Unconnect button was clicked
- (IBAction)didPressLink:(id)sender
{
    // If allready linked, unlink user from app
    if ( [[DBSession sharedSession] isLinked] )
    {
        [[DBSession sharedSession] unlinkAll];
        [self updateLinkButton];
        NSLog( @"App is unlinked." );
    }
    // Else, link app with user
    else
    {
        NSLog( @"Linking app ..." );
        [[DBAuthHelperOSX sharedHelper] authenticate];
    }
}

// Upload file method
- (void)uploadFile:(NSURL *)fileUrl
{
    NSString *localPath = [fileUrl path];
    NSString *filename = [fileUrl lastPathComponent];
    NSString *destDir = @"/";    
    
    [[self restClient] uploadFile:filename toPath:destDir
                    withParentRev:nil fromPath:localPath];
}

- (void)getUrl:(NSAppleEventDescriptor *)event withReplyEvent:(NSAppleEventDescriptor *)replyEvent
{
    NSLog( @"Show app button was clicked." );
    
    // Bring app up front
    [NSApp activateIgnoringOtherApps:YES];
}

#pragma mark private methods

// Method is trigger when authentication state changes 
-(void)authHelperStateChangedNotification:(NSNotification *)notification 
{
    if ( [[DBSession sharedSession] isLinked] )
    {
        NSLog( @"I'm linked!" );
        [self updateLinkButton];
    }
}

- (void)updateLinkButton
{
    if ( [[DBSession sharedSession] isLinked] )
    {
        self.linkButton.title = @"Unlink Dropbox";
    }
    else
    {
        self.linkButton.title = @"Link Dropbox";
        self.linkButton.state = [[DBAuthHelperOSX sharedHelper] isLoading] ? NSOffState : NSOnState;
    }
}

- (DBRestClient *)restClient
{
    if ( ! restClient)
    {
        restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
        restClient.delegate = self;
    }
    return restClient;
}

// Callback for Dropbox successfuly uploaded file
- (void)restClient:(DBRestClient*)client uploadedFile:(NSString*)destPath
              from:(NSString*)srcPath metadata:(DBMetadata*)metadata
{
    NSLog( @"File was successfuly uploaded to path: %@", [metadata path] );
    // Load link for file
    [[self restClient] loadSharableLinkForFile:[metadata path]];
}

// Callback for Dropbox uploaded file with error
- (void)restClient:(DBRestClient*)client uploadFileFailedWithError:(NSError*)error {
    NSLog(@"File upload failed with error - %@", error);
}

// Callback for Dropbox loaded link file
- (void)restClient:(DBRestClient*)restClient loadedSharableLink:(NSString*)link
           forFile:(NSString*)path
{
    NSLog(@"File link: %@.", link);
    
    // Clear contents of paste board
    [pboard clearContents];
    // Write link to pboard
    [pboard setString:link forType:NSStringPboardType];
    
    // Show notification with instruction
    [GrowlApplicationBridge notifyWithTitle:@"File was successfuly uploaded"
                                description:@"Link to your file is in the clipboard now. "
                           notificationName:@"aBoxMonsterFileLink"
                                   iconData:nil
                                   priority:0
                                   isSticky:NO
                               clickContext:nil];
    
}

// Callback for Dropbox loaded link file with error
- (void)restClient:(DBRestClient*)restClient loadSharableLinkFailedWithError:(NSError*)error
{
    NSLog(@"Error %@",error);
}

#pragma NSPanele delegate


@end
